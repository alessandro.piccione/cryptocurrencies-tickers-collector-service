﻿namespace TickersCollector.Main.Models

type Exchange (name:string) =
    member this.Name with get() = name

   