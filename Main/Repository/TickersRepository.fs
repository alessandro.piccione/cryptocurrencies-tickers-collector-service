﻿namespace TickersCollector.Main.Repository

open System

open Alex75.Common.MongoDB
open TickersCollector.Main.Contracts
open TickersCollector.Main.Models


type TickersRepository (mongoHelper) =
    inherit Alex75.Common.MongoDB.MongoRepositoryBase<CollectedTickers>(mongoHelper, "Tickers")
    
    let TTL  = System.TimeSpan.FromDays(365.)

    do       
        base.CreateIndex( (fun t -> t.When :> obj), IndexDirection.Descernding, Nullable(TTL)) |> ignore
        base.CreateIndex( (fun t -> t.Pair :> obj), IndexDirection.Ascending) |> ignore


    interface ITickersRepository with
        member this.Insert collectedTickers: unit = 
            base.Create collectedTickers

        