#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.


ARG VERSION=0.0

FROM mcr.microsoft.com/dotnet/core/runtime:3.1-buster-slim AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src

# copy all the content in the local solution folder to the machine /src folder
COPY . .

RUN dotnet restore "./Cryptocurrencies Tickers Collector Service.sln"

#COPY ["Service Manager/Service Manager.fsproj", ""]
#RUN dotnet restore "./Service Manager.fsproj"
#COPY . .
#WORKDIR "/src/."
## do we need to build or publish is enough ?
#RUN dotnet build "./Service Manager/Service Manager.fsproj" -c Release -o /app/build


FROM build AS publish
RUN dotnet publish "./Service Manager/Service Manager.fsproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
# copy the /app/publish content of publish image to the current /app folder of final image
COPY --from=publish /app/publish .

ENTRYPOINT ["dotnet", "Cryptocurrencies.TickersCollector.Service.dll"]