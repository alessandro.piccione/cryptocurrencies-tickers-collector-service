﻿namespace TickersCollector.Main

open System
open System.Linq
open System.Collections.Generic

open Serilog
open Alex75.Cryptocurrencies
open TickersCollector.Main.Contracts
open TickersCollector.Main.Models

type public CollectorJob (logger:ILogger, tickersReader:IExchangesTickerReader, tickerRepository:ITickersRepository) =

    let pairs = [
        CurrencyPair.XRP_EUR
        CurrencyPair.XRP_USD
        CurrencyPair.ETH_USD
        CurrencyPair.ETH_EUR
        CurrencyPair.BTC_USD
        CurrencyPair.BTC_EUR
        ]

    let mutable isRunning = false


    let CalculateArbitrage (exchangeTickers:ExchangeTicker[]) =
        if exchangeTickers.Length < 2 then Nullable<Arbitrage>()
        else
            let mutable minAsk = exchangeTickers.[0].AskPrice
            let mutable minAskExchange = exchangeTickers.[0].Exchange
            let mutable maxBid = exchangeTickers.[0].BidPrice
            let mutable maxBidExchange = exchangeTickers.[0].Exchange

            for et in exchangeTickers.Skip(1) do
                if et.AskPrice < minAsk then 
                    minAsk <- et.AskPrice
                    minAskExchange <- et.Exchange
                if et.BidPrice > maxBid then
                    maxBid <- et.BidPrice
                    maxBidExchange <- et.Exchange

            let percent = (maxBid - minAsk) / minAsk*100.

            Nullable({MinAsk=minAsk; MinAskExchange=minAskExchange; MaxBid=maxBid; MaxBidExchange = maxBidExchange; Percent=percent})

    interface IJob with

        member this.Execute(): unit =    
            if isRunning then logger.Warning("The previous process is still running. Skip.")
            else
                let timer = System.Diagnostics.Stopwatch.StartNew()
                logger.Information "Collector job start"
            
                for pair in pairs do
                    
                    let tickersMap = 
                        tickersReader.GetTickers pair 
                        |> Map.filter (fun name ticker ->
                            match ticker with 
                            | Ok t -> true 
                            | NotSupported -> false
                            | Error e -> 
                                Diagnostic.IncreaseErrors()
                                logger.Error(sprintf "Failed to read Ticker from %s for %O. %O" name pair e); false )

                    let exchangeTickers = [| 
                        for x in tickersMap -> 
                            let ticker = x.Value.Ticker
                            { Exchange=x.Key; BidPrice= (float)ticker.Bid; AskPrice= (float)ticker.Ask} 
                    |]   
                    
                    let arbitrage = CalculateArbitrage exchangeTickers

                    let collection:CollectedTickers = {
                        When = DateTime.UtcNow
                        Pair = pair.Slashed
                        MainCurrency = pair.Main.UpperCase
                        OtherCurrency = pair.Other.UpperCase
                        Tickers = exchangeTickers
                        Arbitrage = arbitrage
                    }

                    try 
                        tickerRepository.Insert collection
                    with exc ->
                        Diagnostic.IncreaseErrors()
                        logger.Error(exc, "Failed to write Ticker into database.")
                        


                timer.Stop()
                logger.Information(sprintf"Collector job end [Time=%i]" timer.ElapsedMilliseconds)
