﻿module TickersCollector.Service.Program

open System
open Microsoft.Extensions.Hosting
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Configuration
open Serilog
open Serilog.Formatting.Json
open Serilog.Sinks.RollingFileAlternate

open Alex75.Cryptocurrencies
open Alex75.Common.MongoDB

open TickersCollector.Main
open TickersCollector.Main.Contracts
open TickersCollector.Main.Repository

let SetupLogger logPath =

    let logLevelSwitch = Core.LoggingLevelSwitch(Events.LogEventLevel.Information)
    let fileSizeLimit:Nullable<int64> = Nullable(1L*1024L*1024L) // 1 MB // when the file size is reached IT STOPS TO LOG !!!
    let flushInterval = Nullable(TimeSpan.FromSeconds(5.))
    let buffered = true 
    let shared = false
    let retainedFileCountLimit = Nullable<int>(50)
    let messageTemplate = "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}"
        
    let logger = 
        LoggerConfiguration()
            .MinimumLevel.Information()                           
            .WriteTo.RollingFileAlternate(new JsonFormatter(), logPath, Events.LogEventLevel.Information,
                fileSizeLimit, retainedFileCountLimit)
            //.WriteTo.File(new JsonFormatter(), logPath, Events.LogEventLevel.Information, 
            //    fileSizeLimit, logLevelSwitch, buffered, shared, flushInterval, RollingInterval.Day)
            .CreateLogger()             
    
    Log.Logger <- logger


let CreateHostBuilder (args:string[]) = 

    let version = if args.Length > 1 then args.[1] else "0.0"
    Diagnostic.Version <- version

    let CreateKrakenClientApiClient(configuration:IConfiguration) = 
        Alex75.KrakenApiClient.Client(configuration.["Exchanges:Kraken:public key"], configuration.["Exchanges:Kraken:secret key"])
        //:> KrakenApiClient.IClient

    let CreateBinanceApiClient(configuration:IConfiguration) =
        Alex75.BinanceApiClient.Client({
            TickerCacheDuration=TimeSpan.FromSeconds(5.)
            PublicKey=configuration.["Exchanges:Binance:public key"] 
            SecretKey=configuration.["Exchanges:Binance:secret key"] }
        )

    let CreateCexioApiClient(configuration:IConfiguration) =
        Alex75.CexioApiClient.Client(
            configuration.["Exchanges:CEX.IO:user ID"],
            configuration.["Exchanges:CEX.IO:public key"],
            configuration.["Exchanges:CEX.IO:secret key"] 
        )
        


    Host.CreateDefaultBuilder()        
        .ConfigureHostConfiguration( fun(confBuilder:IConfigurationBuilder) ->
        //    // is it ok here to read more settings, like "serilog.json" for example ?
            confBuilder.AddUserSecrets("Cryptocurrencies.TickersCollector.service-3D03CCE5-6551-4D12-96E1-E2F2D17E6E90") |> ignore
        )
        // no way to have this method available utside of a SDK.Web project type
        //.ConfigureWebHostDefaults(fun webBuilder -> ())
        .ConfigureServices( 
            fun (hostContext:HostBuilderContext) (services:IServiceCollection) ->
                
                let logPath = hostContext.Configuration.["Logging:Path"]
                SetupLogger logPath                

                //Alex75.Common.WebApiHosting.HostCreator.CreateApiHost("http://localhost:5123", services)

                /// Web API service
                TickersCollector.Api.HostCreator.CreateApiHost("http://localhost:5123", services)                
                              


                let interval = TimeSpan.Parse(hostContext.Configuration.["Service:Run:interval"])
                let connectionString = hostContext.Configuration.["MongoDB:ConnectionString"]
                let databaseName = hostContext.Configuration.["MongoDB:Database"]

                let krakenApi = CreateKrakenClientApiClient(hostContext.Configuration)
                let binanceApi = CreateBinanceApiClient(hostContext.Configuration)
                let cexioApi = CreateCexioApiClient(hostContext.Configuration)

                services
                    .AddHostedService<Worker>() 
                    .AddSingleton<Serilog.ILogger>(Log.Logger)
                    .AddSingleton<IJobScheduler, CollectorJobScheduler>() 
                    .AddSingleton( {
                        version=version
                        interval=interval 
                        runImmediately=true} ) 
                    .AddSingleton<IJob, CollectorJob>() 

                    // Exchanges API
                    .AddSingleton<IExchangesTickerReader, ExchangeTickerReader>()
                    .AddSingleton(Map([
                        (ExchangeNames.Kraken, krakenApi :> IApiClientPublic)
                        (ExchangeNames.Binance, binanceApi :> IApiClientPublic)
                        (ExchangeNames.CexIO, cexioApi :> IApiClientPublic)
                    ]))

                    // MongoDB
                    .AddSingleton<ITickersRepository, TickersRepository>()
                    .AddSingleton<IMongoHelper>( MongoHelper(connectionString, databaseName) )
                    |> ignore    
            )  
            


[<EntryPoint>]
let main(args:string[]) =  

    CreateHostBuilder(args).Build().Run();    

    exit 0
