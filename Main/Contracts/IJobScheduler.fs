﻿namespace TickersCollector.Main.Contracts

type IJobScheduler = 
    abstract member Start: unit -> unit
    abstract member Stop: unit -> unit