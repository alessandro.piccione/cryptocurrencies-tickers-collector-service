# Cryptocurrencies Tickers Collector Service

Background service that collects buy and sell prices (Tickers) of different currency pairs (Symbols) of different cryptocurrency exchanges.
It stores the collected data into MongoDB.
It will run a SignalR service to publishing data to the subscribers.
It exposes web API to monitor the process health.



## Test Docker locally

Build the image with this command from the solution folder: 
``docker build --build-arg VERSION=0.1-test -t crypto-tickers-collector-service:local .``
and verify the images is created with:  
``doker images``  

Run it 
- interactive  
  ``docker run -it -e MongoDB:ConnectionString="123" -e Logging:Path="/var/log/tickers-collector/.log" --name=tickers-collector crypto-tickers-collector-service:local``
- daemon  
  ``docker run  -d -e MongoDB:ConnectionString="123" -e Logging:Path="/var/log/tickers-collector/.log" -v tickers-collector:/var/log/tickers-collector --name=tickers-collector crypto-tickers-collector-service:local``
  ``docker run  -d -e MongoDB:ConnectionString="123" -e Logging:Path="/var/log/tickers-collector/.log" -v c:/logs/tickers-collector:/var/log/tickers-collector --name=tickers-collector crypto-tickers-collector-service:local``



Attach to a running container
``docker exec -it tickers-collector /bin/bash``
``ls /var/log``
``cat /var/log/ti``

## Web API

http://localhost:5123

- GET /ping

- GET /health
  Response: 
    type: application/text or application/json depending from request Accept-Content-Type HEADER

- GET /diagnostic




## SignalR

### Subscribe/Unsubscribe

API: /collector-hub/subscribe/{connectionId}/{main}/{other}


## MongoDB

### Delete error/teemporary records

Connect with mongo shell and set the database:  
mongo "mongodb+srv://cluster0-bla2o.mongodb.net/test" --username <username>  
``use TickersCollector_dev``  

- count documents in "Tickers" collection:
  ``db.Tickers.count()``

- find, see and delete documents that doesn't have "" field:
  ``db.Tickers.find({"When": {$exists: false}}).count()``
  ``db.Tickers.find({"When": {$exists: false}}).limit(10).pretty()``
  ``db.Tickers.deleteMany({"When": {$exists: false}})``

- see newer, older documents:
  ``db.Tickers.find({}).sort({"When": -1}).limit(10).pretty()``  // desc

- see Arbitrage value for XRP/EUR pair:
  ``db.Tickers.find({"Pair":"XRP/EUR"}, {"_id":0, "When":1, "Pair":1, "Arbitrage":1}).sort({"When": -1}).limit(10).pretty()``


- see records with Arbitrage value higher than 1%:
  ``db.Tickers.find({"Arbitrage.Percent": {$gt: 1}}, {"_id":0, "When":1, "Pair":1, "Arbitrage":1}).sort({"When": -1}).limit(10).pretty()``
  ``db.Tickers.find({"Arbitrage.Percent": {$gt: 1}}, {"_id":0, "When":1, "Pair":1, "Arbitrage":1, "p": "aaa" }).sort({"When": -1}).limit(10).pretty()``
  ```db.Tickers.aggregate([
    { $match: {"Arbitrage.Percent": {$gt: 1}} },
    { $project:{
        "_id":0,
        "Pair":1,
        "When": 0,
        "Time": "$When",
        "When": { $dateToString: { date: "$When", format: "%H:%M:%S:%L%z" } },
        "Arbitrage %": { $round: ["$Arbitrage.Percent", 1]},
        "Buy": { $concat: [ "$Arbitrage.MinAskExchange", " @ ", {$toString:"$Arbitrage.MinAsk"}] },
        "Sell": { $concat: [ "$Arbitrage.MaxBidExchange", " @ ", {$toString:"$Arbitrage.MaxBid"}] }
    } },
    { $sort: {"Pair":1, "When":-1}}
  ])
  ```
  find({"Arbitrage.Percent": {$gt: 1}}).aggregate([]).sort({"When": -1}).limit(10).pretty()``


### Problems

- (Docker) ARG accept only one argument
