﻿namespace TickersCollector.Service

open System.Threading
open System.Threading.Tasks
open Microsoft.Extensions.Hosting

open TickersCollector.Main.Contracts


type Worker (collectorJobScheduler:IJobScheduler) =
    inherit BackgroundService()
    
    member this.Start() = 
        collectorJobScheduler.Start()


    override this.ExecuteAsync (step:CancellationToken):Task =
        //Task.FromResult(0)
        new Task( fun () -> () )
