namespace TickersCollector.Api

open System.Collections.Generic
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Hosting
open Microsoft.AspNetCore.Mvc
open Microsoft.AspNetCore.Hosting

type HostCreator () =        

    static member CreateApiHost(url:string, services:IServiceCollection) =

        Host.CreateDefaultBuilder()
            .ConfigureWebHostDefaults(fun webBuilder ->
                webBuilder
                    .UseUrls(url)
                    .UseStartup<Startup>()
                    //.ConfigureServices( fun context services ->
                    //    ()
                    //    //services.
                    //)
                |> ignore
            )
            .Build().Start()