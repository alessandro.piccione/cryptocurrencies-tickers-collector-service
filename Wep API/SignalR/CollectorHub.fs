﻿namespace TickersCollector.SignalR

open Microsoft.Extensions.Logging
open Microsoft.AspNetCore.SignalR
open Microsoft.AspNetCore.Authorization

//[<Authorize>]
type CollectorHub (logger : ILogger<CollectorHub>) =
    inherit Hub()


    member me.JoinGroup (group:string) =
        logger.LogInformation(sprintf "JoinGroup %s (ConnectionId: %s)" group base.Context.ConnectionId)
        base.Groups.AddToGroupAsync(base.Context.ConnectionId, group)

    member me.LeaveGroup (group:string) =
        logger.LogInformation(sprintf "LeaveGroup %s (ConnectionId: %s)" group base.Context.ConnectionId)
        base.Groups.RemoveFromGroupAsync(base.Context.ConnectionId, group)
        
