﻿namespace TickersCollector.Main

open System
open Serilog
open TickersCollector.Main.Contracts

type SchedulerConfiguration = {version:string; interval:TimeSpan; runImmediately:Boolean}


type CollectorJobScheduler(logger:ILogger, config:SchedulerConfiguration, job:IJob) =
    
    let timer = new Timers.Timer(config.interval.TotalMilliseconds)    
    let timerElapsed = fun args -> job.Execute()

    do
        logger.Information("Start (from creation) v." + config.version)
        logger.Information(sprintf "Run every %i seconds" ((int)timer.Interval/1000))
        timer.AutoReset <- true
        timer.Elapsed.Add(timerElapsed)
        if config.runImmediately then timerElapsed()        
        timer.Start()


    interface IJobScheduler with

        member _.Start() = 
            logger.Information "Start"
            timer.Enabled <- true  

        member _.Stop() = 
            logger.Information "Stop"
            timer.Enabled <- false
