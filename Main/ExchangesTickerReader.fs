﻿namespace TickersCollector.Main
 
open System
open Alex75.Cryptocurrencies
open ExchangeNames


type TickerResult =
    | Ok of Ticker
    | NotSupported
    | Error of Exception    
    member this.Ticker with get() = 
        match this with 
        | Ok t -> t 
        | NotSupported -> failwith "Result is NotSupported"
        | Error e -> failwith ("Result is Error: " + e.ToString())

type IExchangesTickerReader =

    abstract GetTickers: CurrencyPair ->  Map<string, TickerResult>
    abstract member FailedToReadTicker: Event<string * string * Exception>


type ExchangeTickerReader (clients:Map<string, IApiClientPublic>) =

    let onFailed = new Event<string * string * Exception>()    

    let HasPair exchange pair =
        if exchange = Binance then 
            if Array.contains pair [|CurrencyPair.BTC_EUR; CurrencyPair.ETH_EUR; CurrencyPair.XRP_EUR; |] then true else false
        else true

    interface IExchangesTickerReader with

        member this.FailedToReadTicker: Event<string * string * Exception> = onFailed

        member this.GetTickers pair =   
            let getTicker name (client:IApiClientPublic): TickerResult = 

                if HasPair name pair then
                    try
                        let ticker = client.GetTicker pair
                        TickerResult.Ok(ticker)
                    with
                    | :? UnsupportedPair as e ->
                        onFailed.Trigger(name, pair.Slashed, e :> Exception)
                        TickerResult.Error(e :> Exception)
                    | e ->
                        onFailed.Trigger(name, pair.Slashed, e)
                        TickerResult.Error(e)
                else 
                    TickerResult.NotSupported

            clients |> Map.map getTicker 