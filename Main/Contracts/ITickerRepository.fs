﻿namespace TickersCollector.Main.Contracts

open TickersCollector.Main.Models

type ITickersRepository =
    abstract member Insert: CollectedTickers -> unit 