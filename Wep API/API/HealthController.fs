﻿namespace TickersCollector.Api.WebApi

open Microsoft.Extensions.Logging
open Microsoft.AspNetCore.Mvc

[<ApiController>]
[<Route("/health")>]
type public HealthController (logger : ILogger<HealthController>) =
    inherit ControllerBase()
        

    [<HttpGet; Route("")>]
    member this.Health() = 
        match this.Request.ContentType with
        | "application/json" ->
            let result = {| status = "ok" |}  // Anonymous Record
            JsonResult( result ) :> IActionResult
        | _ -> this.Ok("ok") :> IActionResult

