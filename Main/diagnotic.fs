﻿namespace TickersCollector.Main

open System
open System.Timers

type TimeWindowCounter(timeSpan:TimeSpan) =
    
    let mutable value:int = 0  
    let values = System.Collections.Concurrent.ConcurrentStack<DateTime>()

    member this.Value with get() = 
        if values.IsEmpty then 0
        else 
            lock values ( fun () ->
                let minTime = DateTime.UtcNow - timeSpan  
                let current:DateTime ref = ref DateTime.MinValue
                while values.TryPeek(current) && current.Value < minTime && values.TryPop(current) do ()  
            )        
            values.Count

    member this.Increase () =
        values.Push(DateTime.UtcNow)



type public Diagnostic() =

    static let startedOn = DateTime.UtcNow
    static let mutable version = ""

    static let mutable errors = 0
    static let last24hErrors = TimeWindowCounter(TimeSpan.FromHours(24.))
    static let last1hErrors = TimeWindowCounter(TimeSpan.FromHours(1.))


    static member StartedOn = startedOn
    static member Version with get() = version and set(value) = version <- value
    //static member RunEveryNSeconds = 
    static member Errors = errors
    static member Last24hErrors = last24hErrors.Value
    static member Last1hErrors = last1hErrors.Value

    static member IncreaseErrors () = 
        errors <- (errors + 1)
        last24hErrors.Increase()
        last1hErrors.Increase()

    
