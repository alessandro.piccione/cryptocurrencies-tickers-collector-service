﻿namespace TickersCollector.Api.WebApi

open Microsoft.Extensions.Logging
open Microsoft.AspNetCore.Mvc
open TickersCollector.Main

[<ApiController>]
[<Route("/diagnostic")>]
type public DiagnosticController (logger : ILogger<DiagnosticController>) =
    inherit ControllerBase()        

    [<HttpGet; Route("")>]
    member this.Diagnostic() = 
        match this.Request.ContentType with
        | "application/json" ->
            let result = {| // Anonymous Record
                startedOn = Diagnostic.StartedOn
                version = Diagnostic.Version
                status = "ok"
                errors = Diagnostic.Errors
                errorsLast24h = Diagnostic.Last24hErrors
                errorsLast1h = Diagnostic.Last1hErrors
                |}  
            JsonResult( result ) :> IActionResult
        | _ -> 
            this.Ok(sprintf """
                startedOn = %A
                version = %s
                status = "ok"
                errors = %d
                errorsLast24h = %d
                errorsLast1h = %d
                """
                Diagnostic.StartedOn
                Diagnostic.Version
                Diagnostic.Errors
                Diagnostic.Last24hErrors
                Diagnostic.Last1hErrors

            ) :> IActionResult

