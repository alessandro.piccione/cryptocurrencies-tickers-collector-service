﻿namespace TickersCollector.Api.WebApi

open Microsoft.Extensions.Logging
open Microsoft.AspNetCore.Mvc
open Microsoft.AspNetCore.SignalR
open TickersCollector.SignalR
open Alex75.Cryptocurrencies
open System

[<ApiController; Route("/api/collector")>]
type public SignalRCollectorHubController (logger:ILogger<SignalRCollectorHubController>,
                                    collectorHub:IHubContext<CollectorHub>) =
    inherit ControllerBase()
       
    let ErrorResult(statusCode:int, error:string) =        
        let data = {| status = "error"; message = error |}  // Anonymous Record
        //JsonResult( result ) :> IActionResult
        let result = ObjectResult(data)
        result.StatusCode <- Nullable(500)
        result :> IActionResult

    [<HttpPost; Route("subscribe/{connectionId}/{main}/{other}")>]    
    member this.Subscribe(connectionId, main:string, other:string):IActionResult = 

        let pair = CurrencyPair(main, other)
        logger.LogInformation(sprintf "Subscribe to pair. Pair: %O. ConnectionId:%s." pair connectionId)
             
        try             
            let group = pair.Slashed
            collectorHub.Groups.AddToGroupAsync(connectionId, group) |> Async.AwaitTask |>  ignore
            OkResult() :> IActionResult                
        with e -> 
            logger.LogError(e, sprintf "Failed to subscribe to pair. Pair: %O. ConnectionId:%s." pair connectionId)
            ErrorResult(500, sprintf "Failed to subscribe to pair. Pair: %O. ConnectionId:%s." pair connectionId)
            
    [<HttpPost("unsubscribe/{connectionId}/{main}/{other}")>]
    member this.UnSubscribe connectionId (main:string) (other:string) = 

        let pair = CurrencyPair(main, other)
        logger.LogInformation(sprintf "Unsubscribe to pair. Pair: %O. ConnectionId:%s." pair connectionId)
             
        try             
            let group = pair.Slashed
            collectorHub.Groups.RemoveFromGroupAsync(connectionId, group) |> Async.AwaitTask |>  ignore
            OkResult() :> IActionResult              
        with e -> 
            logger.LogError(e, sprintf "Failed to unsubscribe to pair. Pair: %O. ConnectionId:%s." pair connectionId)
            ErrorResult(500, failwithf "Failed to unsubscribe to pair. Pair: %O. ConnectionId:%s." pair connectionId)

