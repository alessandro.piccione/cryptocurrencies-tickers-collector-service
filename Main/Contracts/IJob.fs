﻿namespace TickersCollector.Main.Contracts

type IJob =
    abstract member Execute: unit -> unit
