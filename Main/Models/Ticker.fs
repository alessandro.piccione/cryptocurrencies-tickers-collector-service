﻿namespace TickersCollector.Main.Models

open System

type public ExchangeTicker = {
    Exchange:string
    BidPrice:float
    AskPrice:float
}


[<Struct>]
type public Arbitrage = {
    MaxBid:float
    MaxBidExchange:string
    MinAsk:float
    MinAskExchange:string
    Percent:float
}

type public CollectedTickers = {
    When:System.DateTime
    Pair:string
    MainCurrency:string
    OtherCurrency:string
    Tickers:ExchangeTicker[]
    Arbitrage:Nullable<Arbitrage>
}